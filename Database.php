<?php

/**
 * Created by PhpStorm.
 * User: snaha
 * Date: 23/1/17
 * Time: 10:00 PM
 */
 class Database
{
    function __construct()
    {
        $this->servername = "localhost";
        $this->username = "root";
        $this->password = "toor";
        $this->dbname = "restapi";
        $this->conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
    }

    public function createTable(){
        $this->query("CREATE TABLE `queue` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `func_id` int(11) NOT NULL,
      `params` varchar(200) NOT NULL,
      `status` enum('READY','RUNNING','COMPLETED') DEFAULT 'READY',
      `date_created` datetime NOT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1");
    }

    public function query($sql){

        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
        $result = $this->conn->query($sql);
        if($result===TRUE){
            $last_id = $this->conn->insert_id;
            $affected = mysqli_affected_rows($this->conn)>0;
            return empty($last_id)?$affected:$last_id;
        }elseif($result===FALSE)
            return false;

        if(empty($result))
            return array();
        $data = array();
        while($row = $result->fetch_assoc()) {
            $data[] = $row;
        }
        return sizeof($data)==1? $data[0] : $data;
    }
}?>
