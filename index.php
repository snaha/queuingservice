<?php
/**
 * Created by PhpStorm.
 * User: snaha
 * Date: 23/1/17
 * Time: 9:00 PM
 */
require_once 'api.php';

$base_url = getCurrentUri();
$routes = array();
$raw_routes = explode('/', $base_url);
foreach($raw_routes as $route)
{
    if(trim($route) != '')
        array_push($routes, $route);
}

if($routes[0] == "api")
	{
        if($routes[1] == "task")
		{
            $action = $routes[2];
            $method = $_SERVER['REQUEST_METHOD'];
            $action =$action.'_'.strtolower($method);
            $task =new Task();
            if(method_exists($task, $action)){
                $response = $task->{$action}();
                show_json($response);
            }else
                show_json(array('s'=>false,'message'=>'404 Not Found'), true);
        }
}

function getCurrentUri() {
    $basepath = implode('/', array_slice(explode('/', $_SERVER['SCRIPT_NAME']), 0, -1)) . '/';
    $uri = substr($_SERVER['REQUEST_URI'], strlen($basepath));
    if (strstr($uri, '?')) $uri = substr($uri, 0, strpos($uri, '?'));
    $uri = '/' . trim($uri, '/');
    return $uri;
}
 function show_json($data, $is_404 = false){
    if($is_404)
        header("HTTP/1.1 400 NOT FOUND");
    else
        header("HTTP/1.1 200 OK");
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400'); // cache for 1 day
    }
    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    echo json_encode($data);
}