<?php
/**
 * Created by PhpStorm.
 * User: snaha
 * Date: 23/1/17
 * Time: 9:12 PM
 */
require_once 'Queue.php';
class Task{

    public function add_post(){

        $func_id = $_POST['func_id'];
        $params = empty($_POST['params'])?array():json_decode($_POST['params']);
        if(empty($func_id))
            return array('success'=>false,'message'=>'Function ID is required');

        if(!empty($params) && (empty($params->first_name) || empty($params->user_id)))
            return array('success'=>false,'message'=>'Function params are wrong!');

        if(!is_numeric($func_id))
            return array('success'=>false,'message'=>'Function ID should be integer');

        $queue = new Queue(new Database);
        return $queue->enQueue(array('func_id'=>$func_id, 'params'=>$params));
    }

    public function fetch_get(){
        $queue = new Queue(new Database);
        return $queue->deQueue();
    }

    public function completed_post(){
        $task_id = $_POST['task_id'];
        if(empty($task_id))
            return array('success'=>false,'message'=>'Task ID is required');

        $queue = new Queue(new Database);
        return $queue->makeQueueElementCompleted($task_id);
    }

}