<?php

/**
 * Created by PhpStorm.
 * User: snaha
 * Date: 24/1/17
 * Time: 12:20 AM
 */
require_once 'Database.php';
class Queue
{
    public function __construct(Database $db)
    {
        $this->db = $db;
    }

    public function enQueue($data = array()){
        if(empty($data))
            return array('success'=>false);
        $func_id = $data['func_id'];

        $select = $this->db->query("select id from queue where func_id = '$func_id' ");
        if(sizeof($select)>0)
            return array('success'=>false,'message'=>'Function already exists');

        $date = date('Y-m-d H:i:s');
        $params = json_encode($data['params']);
        $task_id = $this->db->query("insert into queue (`func_id`,`params` ,`date_created`) values ( '$func_id', '$params' , '$date' )  ");

        if($task_id){
            return array('success'=>true, 'task_id'=>$task_id);
        }else
            return array('success'=>false);
    }

    public function deQueue(){
        $task = $this->db->query("select id,func_id,params from queue where status = 'READY' order by id ASC limit 1");
        if(empty($task))
            return array('success'=>false);
        $id = $task['id'];
        $this->db->query("update queue set status = 'RUNNING' where id = $id ");
        return array_merge(array('success'=>true), $task);
    }

    public function makeQueueElementCompleted($task_id){
        $task = $this->db->query("update queue set status = 'COMPLETED' where id = $task_id ");
        if(empty($task))
            return array('success'=>false);
        return array('success'=>true);
    }
}